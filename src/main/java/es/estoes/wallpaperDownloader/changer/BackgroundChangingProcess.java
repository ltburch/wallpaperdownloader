/**
 * Copyright 2016-2018 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.changer;

import javax.swing.SwingWorker;
import org.apache.log4j.Logger;
import es.estoes.wallpaperDownloader.util.PreferencesManager;
import es.estoes.wallpaperDownloader.util.WDUtilities;

/**
* This class implements SwingWorker abstract class. It is used to execute a independent 
* Thread from the main one (Event Dispatch Thread) which renders the GUI, listens to the
* events produced by keyboard and elements of the GUI, etc...
* The Thread created by SwingWorker class is a Thread in which a heavy process can be 
* executed in background and return control to EDT until it finishes
* More information: http://chuwiki.chuidiang.org/index.php?title=Ejemplo_sencillo_con_SwingWorker
 */
public class BackgroundChangingProcess extends SwingWorker<Void, Void> {

	// Constants
	private static final Logger LOG = Logger.getLogger(BackgroundChangingProcess.class);
	
	// Attributes
	private Long timer;
	private Boolean changeWallpaper;
	
	// Getters & Setters
	/**
	 * Gets changeWallpaper.
	 * @return changeWallpaper
	 */
	public Boolean getChangeWallpaper() {
		return this.changeWallpaper;
	}

	/**
	 * Sets changeWallpaper.
	 * @param changeWallpaper changeWallpaper
	 */
	public void setChangeWallpaper(Boolean changeWallpaper) {
		this.changeWallpaper = changeWallpaper;
	}	

	// Methods
	/**
	 * This method executes the background process and returns control to EDT.
	 * @return 
	 */
	@Override
	protected Void doInBackground() throws Exception {
		// When the process starts, the linked list of available wallpapers is removed in order
		// to build it again freshly
		if (LOG.isInfoEnabled()) {
			LOG.info("Available wallpapers linked list is removed in order to build it again freshly if it is necessary");
		}
		WDUtilities.getWallpaperChanger().setWallpapers(null);
		
		PreferencesManager prefm = PreferencesManager.getInstance();
		this.setChangeWallpaper(Boolean.TRUE);
		// Getting timer
		if (prefm.getPreference("application-changer").equals("0")) {
			// Changer is disabled
			this.setChangeWallpaper(Boolean.FALSE);
			if (LOG.isInfoEnabled()) {
				LOG.info("Automated wallpaper changer process is OFF");
			}
		} else {
			// Changer is enabled
			Integer timeInterval = new Integer(prefm.getPreference("application-changer"));
			String timeUnit = prefm.getPreference("application-changer-units");
			switch (timeUnit) {
			case "s":
				this.timer = Long.valueOf(timeInterval * 1000);
				break;
			case "m":
				this.timer = Long.valueOf(timeInterval * 60 * 1000);
				break;
			case "h":
				this.timer = Long.valueOf(timeInterval * 60 * 60 * 1000);
				break;
			default:
				this.timer = Long.valueOf(timeInterval * 1000);
				break;
			}
			LOG.info("Changing timer set every " + this.timer + " milliseconds");
			// 1.- Getting random wallpaper
			// 2.- Set wallpaper depending on the mode
			// 3.- Starting again after timer is consumed
			while (this.getChangeWallpaper()) {
				String changerMode = prefm.getPreference("application-changer-mode");
				switch (changerMode) {
				case "0":
					// The wallpaper will be changed randomly
					if (LOG.isInfoEnabled()) {
						LOG.info("Changer mode is random.");
					}
					WDUtilities.getWallpaperChanger().setRandomWallpaper();
					break;
				case "1":
					// The wallpaper will be changed from oldest to newest
					if (LOG.isInfoEnabled()) {
						LOG.info("Changer mode is from oldest to newest.");
					}
					WDUtilities.getWallpaperChanger().setSortedWallpaper();
					break;
				case "2":
					// The wallpaper will be changed from newest to oldest
					if (LOG.isInfoEnabled()) {
						LOG.info("Changer mode is from newest to oldest.");
					}
					WDUtilities.getWallpaperChanger().setSortedWallpaper();
					break;
				default:
					// The wallpaper will be changed randomly
					if (LOG.isInfoEnabled()) {
						LOG.info("Changer mode is unknown.");
					}
					WDUtilities.getWallpaperChanger().setRandomWallpaper();
					break;
				}
				Thread.sleep(this.timer);
			}
		}
		return null;
	}
}
